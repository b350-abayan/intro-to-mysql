-- Insert multiple users 
INSERT INTO users (email, password, datetime_created) VALUES 
("johnsmith@gmail.com", "passwordA",20210101010000),
("juandelacruz@gmail.com", "passwordB",20210101020000),
("janesmith@gmail.com", "passwordC",20210101030000),
("mariadelacruz@gmail.com", "passwordD",20210101040000),
("johndoe@gmail.com", "passwordE",20210101050000);

-- Insert multiple posts 
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES 
(1, "First Code", "Hello World!",20210201010000),
(1, "Second Code", "Hello Earth!",20210201020000),
(2, "Third Code", "Welcome Mars!",20210201030000),
(4, "Fourth Code", "Bye bye solar system",20210201040000);

-- Get all the post with an Author ID of 1
SELECT * FROM posts WHERE author_id = 1;

-- Get all the user's email and datetime of creation
SELECT email, datetime_created FROM users;

-- Update a post's content to "Hello to the people of the Earth!” where its initial content is "Hello Earth!" by using the record's ID
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

-- Delete the user with an email of "johndoe@gmail.com"
DELETE FROM users WHERE email = "johndoe@gmail.com";


-- Stretch Goals
-- #1
INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES 
("johndoe", "john1234", "John Doe", "09123456789", "john@mail.com", "Quezon City");

-- #2
INSERT INTO playlists (user_id, datetime_created) VALUES 
(1, 20230920080000);

-- #3
INSERT INTO playlists_songs (playlist_id, song_id) VALUES 
(1, 1), (1, 2);
